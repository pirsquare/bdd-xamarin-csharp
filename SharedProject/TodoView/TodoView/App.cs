﻿using System;
using Xamarin.Forms;
using TodoModel;

namespace TodoView
{
	public class App
	{
		public static Page GetMainPage ()
		{
			database = new TodoItemDatabase();

//			var todoItem1 = new TodoModel.TodoItem();
//			todoItem1.ID = 0;
//			todoItem1.Name = "4545";
//			todoItem1.Notes = "4545";
//
//			database.SaveItem (todoItem1);
//			Console.WriteLine ("todoItem1:");
//			Console.WriteLine (todoItem1.ID);
//			Console.WriteLine (todoItem1.Name);
//			Console.WriteLine (todoItem1.Notes);
//
//			var todoItem2 = database.GetItem (todoItem1.ID);
//			Console.WriteLine ("todoItem2:");
//			Console.WriteLine (todoItem2.ID);
//			Console.WriteLine (todoItem2.Name);
//			Console.WriteLine (todoItem2.Notes);
//			Console.WriteLine ("test:");
//			var test1 = todoItem2.ID == todoItem1.ID;
//			Console.WriteLine (test1);
//			var test2 = todoItem2.Name == todoItem1.Name;
//			Console.WriteLine (test2);
//			var test3 = todoItem2.Notes == todoItem1.Notes;
//			Console.WriteLine (test3);
//
			var mainNav = new NavigationPage (new TodoListPage ());

			return mainNav;
		}

		static TodoItemDatabase database;
		public static TodoItemDatabase Database {
			get { return database; }
		}
	}
}

