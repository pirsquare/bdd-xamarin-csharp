﻿
namespace TodoView
{
	public interface ITextToSpeech
	{
		void Speak (string text);
	}
}

