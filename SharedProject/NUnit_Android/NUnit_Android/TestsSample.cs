﻿using System;
using NUnit.Framework;

namespace NUnit_Android
{
	[TestFixture]
	public class TestsSample
	{
		static TodoModel.TodoItemDatabase database;
		static TodoModel.TodoItem todoItem1;
		[SetUp]
		public void Setup ()
		{
			database = new TodoModel.TodoItemDatabase ();
			database.DeleteAll ();

			todoItem1 = new TodoModel.TodoItem();
			todoItem1.Name = "one";
			todoItem1.Notes = "first note";
			database.SaveItem (todoItem1);


			database.SaveItem (new TodoModel.TodoItem());
			database.SaveItem (new TodoModel.TodoItem());
			database.SaveItem (new TodoModel.TodoItem());
		}

		[Test]
		public void TestMatchWriteRead ()
		{
			var todoItem2 = database.GetItem (todoItem1.ID);
			Assert.AreEqual (todoItem1.ID, todoItem2.ID);
			Assert.AreEqual (todoItem1.Name, todoItem2.Name);
			Assert.AreEqual (todoItem1.Notes, todoItem2.Notes);
		}

		[Test]
		public void TestGetItems ()
		{
			var list = database.GetItems ();
			Assert.AreEqual (4, list.Count);
		}

		[Test]
		public void TestGetItemsNotDone ()
		{
			var list1 = database.GetItemsNotDone ();
			Assert.AreEqual (4, list1.Count);

			todoItem1.Done = true;
			database.SaveItem (todoItem1);
			var list2 = database.GetItemsNotDone ();
			Assert.AreEqual (3, list2.Count);
		}
	}
}

